terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.41.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

#####################
# IAM Configuration #
#####################

# The first IAM role is to allow access to Lambda.
# Subsequent permissions need to be attached using aws_iam_role_policy_attachment
resource "aws_iam_role" "ddns_exec" {
  name = "lambda-dynamic-dns"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Allowing access to Route53 and also to Cloudwatch for writing logs.
resource "aws_iam_policy" "route53_access" {
  name   = "Route53Access"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:GetHostedZone",
                "route53:ListResourceRecordSets",
                "route53:ListHostedZones",
                "route53:ChangeResourceRecordSets",
                "route53:ListResourceRecordSets",
                "route53:GetHostedZoneCount",
                "route53:ListHostedZonesByName"
            ],
            "Resource": "arn:aws:route53:::hostedzone/${var.ddns-hosted-zone}"
        }
    ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "route53_attach" {
  role       = aws_iam_role.ddns_exec.name
  policy_arn = aws_iam_policy.route53_access.arn
}

########################
# Lambda Configuration #
########################

# First we need to create the lambda.py from a template file
# This essentially just injects variables from Terraform
resource "local_file" "config_lambda" {
  content = templatefile("src/lambda.py.template", {
      hostedZone = var.ddns-hosted-zone
      subdomain  = var.ddns-subdomain
      keys       = "{'test': '123445', 'test2': '123446'}"
    }
  )

  filename = "src/lambda.py"
}

# Next, zip the file up ready for Lambda.
data "archive_file" "ddns" {
  type        = "zip"
  source_file = "src/lambda.py"
  output_path = "src/lambda.zip"

  depends_on  = [local_file.config_lambda]  
}

# Upload it to Lambda as a function
resource "aws_lambda_function" "lambda_ddns" {
  function_name    = "DynamicDNS"
  role             = aws_iam_role.ddns_exec.arn
  runtime          = "python3.8"
  filename         = "src/lambda.zip"
  handler          = "lambda.lambda_handler"
  # Use the output from archive_file to force regeneration
  source_code_hash = data.archive_file.ddns.output_base64sha256
}

#############################################
# Configure an SSL certificate for a domain #
# to attach to the API gateway              #
#############################################

resource "aws_acm_certificate" "api" {
  domain_name               = var.api-hostname
  validation_method         = "DNS"
}

resource "aws_route53_record" "cert_validation" {
  allow_overwrite = true
  name            = tolist(aws_acm_certificate.api.domain_validation_options)[0].resource_record_name
  records         = [ tolist(aws_acm_certificate.api.domain_validation_options)[0].resource_record_value ]
  type            = tolist(aws_acm_certificate.api.domain_validation_options)[0].resource_record_type
  zone_id         = var.api-hosted-zone
  ttl             = 60
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.api.arn
  validation_record_fqdns = [ aws_route53_record.cert_validation.fqdn ]
}

#############################
# API Gateway Configuration #
#############################

resource "aws_apigatewayv2_api" "api" {
  name          = "ddns"
  protocol_type = "HTTP"
}

resource "aws_lambda_permission" "apigw" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_ddns.arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.api.execution_arn}/*/*"
}

resource "aws_apigatewayv2_domain_name" "api" {
  domain_name     = var.api-hostname

  domain_name_configuration {
    certificate_arn = aws_acm_certificate_validation.cert.certificate_arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }
}

resource "aws_apigatewayv2_stage" "ddns" {
  api_id = aws_apigatewayv2_api.api.id
  name        = "ddns"
  auto_deploy = true
}

resource "aws_apigatewayv2_api_mapping" "ddns" {
  api_id      = aws_apigatewayv2_api.api.id
  domain_name = aws_apigatewayv2_domain_name.api.id
  stage       = aws_apigatewayv2_stage.ddns.id
}

resource "aws_apigatewayv2_integration" "ddns" {
  api_id = aws_apigatewayv2_api.api.id

  integration_uri    = aws_lambda_function.lambda_ddns.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "ddns" {
  api_id = aws_apigatewayv2_api.api.id

  route_key = "GET /ddns"
  target    = "integrations/${aws_apigatewayv2_integration.ddns.id}"
}

resource "aws_route53_record" "api-endpoint" {
  name    = aws_apigatewayv2_domain_name.api.domain_name
  type    = "A"
  zone_id = var.api-hosted-zone

  alias {
    name                   = aws_apigatewayv2_domain_name.api.domain_name_configuration[0].target_domain_name
    zone_id                = aws_apigatewayv2_domain_name.api.domain_name_configuration[0].hosted_zone_id
    evaluate_target_health = false
  }
}


output "base_url" {
  description = "Base URL for API Gateway stage."

  value = aws_apigatewayv2_stage.ddns.invoke_url
}
