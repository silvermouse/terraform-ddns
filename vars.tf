variable "ddns-hosted-zone" {
  type         = string
  description  = "The hosted zone for the DynamicDNS entries"
  default      = "ENTER_YOUR_HOSTED_ZONE_HERE"
}

variable "ddns-subdomain" {
  type         = string
  description  = "The subdomain for DynamicDNS entries, with the leading ."
  default      = ".mydomain.com"
}

variable "api-hosted-zone" {
  type         = string
  description  = "The hosted zone for the API hostname. Might be the same as ddns-hosted-zone"
  default      = "ENTER_YOUR_HOSTED_ZONE_HERE"
}

variable "api-hostname" {
  type         = string
  description  = "The hostname to point to the API gateway"
  default      = "apigw.mydomain.com"
}
